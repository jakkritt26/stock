import Vue from 'vue';
import VueRouter from 'vue-router';
import Auth from './middleware/Auth';
// import log from '@/js/middleware/log';
import LoginCheck from './middleware/LoginCheck';

import Home from '@/js/components/Home';
import Login from '@/js/components/Login';
import Products from '@/js/components/Products';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path:'/',
            name : 'Login',
            component: Login,            
        },
        {
            path:'/login',
            name : 'Login',
            component: Login,            
        },
       {
            path:'/home',
            name : 'Home',
            component: Home,
            
        },
        {
            path:'/products',
            name : 'Products',
            component: Products,
            
        }
    ]
});


router.beforeEach((to, from, next) => {
   // console.log(to);
    if (to.name=='Login') {
        if (localStorage.getItem('token')) {
            return next({
              name: 'Home'
           })
        }else{
            return next();
        }
    }else if (to.name != 'Login') {
        if (!localStorage.getItem('token')) {
            return next({
              name: 'Login'
           })
        }else{
            return next();
        }
    }    
    
    // if (to.meta.middleware) {
        
    //     return next()
    // }
    
   //const middleware = to.meta.middleware

    // const context = {
    //     to,
    //     from,
    //     next        
    // }
    // return middleware[0]({
        
    // })
})

export default router;