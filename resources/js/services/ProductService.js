import Api from '@/js/services/ApiService'

export default{
    get_product(){
       // console.log(data);
        return Api().get('product')
    },
    get_product_id($id){
        // console.log(data);
         return Api().get('product/'+$id);
     },
    create_product(data){
        return Api().post('product',data)
    },
    update_product(data){
        return Api().post('product-update',data)
    },
    delete_product(data){
        return Api().post('product-del',data)
    },
    update_stock(data){
        return Api().post('product-stock',data)
    },
}