import Api from '@/js/services/ApiService'

export default{
    login(data){
        console.log(data);
        return Api().post('login',data)
    },
    logout(){
        return Api().post('logout')
    }
}