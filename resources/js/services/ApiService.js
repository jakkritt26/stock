import axios from 'axios';

const ajax = axios.create({
    baseURL:'http://localhost:8000/api/',
  })
 
 ajax.CancelToken = axios.CancelToken
 ajax.isCancel = axios.isCancel

 ajax.interceptors.request.use(
   (config) => {
     let token = localStorage.getItem('token')
 
     if (token) {
       config.headers['Authorization'] = `${ token }`
     }
 
     return config
   },
 
   (error) => {
     return Promise.reject(error)
   }
 )
 
 export default()=>{
    return ajax
 }

// export default()=>{
//     return axios.create({
//         baseURL:'http://localhost:8000/api/',
//     })
// }