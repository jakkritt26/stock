export default function LoginCheck({ next }) {
  
    if (localStorage.getItem('token')) {
      return next({
        name: 'Home'
     })
     // return router.push({ name: 'login' });
    }
   
    return next();
  }