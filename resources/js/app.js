// require('./bootstrap');
import "./bootstrap"
import Vue from 'vue';
// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

//// Router ////
import Routes from '@/js/routes.js';
import App from '@/js/views/App';
import '@/sass/style.css';
const app = new Vue({
    // vuetify : new Vuetify(),
    el : "#app",
    router: Routes,
    render: h => h(App),
//     methods:{
//         getpath() {
//             this.path="";
//            console.log(window.location.pathname);
//        }
      
//    }
})


export default app;
