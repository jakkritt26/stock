<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_stock', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('price')->nullable();
            $table->integer('stock')->default(0);
            $table->string('img')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });


        Schema::create('product_stock_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_id');
            $table->integer('stock')->default(0);
            $table->integer('User');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
