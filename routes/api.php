<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('usercreate','UserController@create');
Route::post('login','LoginController@login')->middleware('cors');
Route::get('user','UserController@get')->middleware('checktoken');
Route::get('product','ProductController@list')->middleware('checktoken');
Route::get('product/{id}','ProductController@get')->middleware('checktoken');
Route::post('product','ProductController@create')->middleware('checktoken');
Route::post('product-update','ProductController@update')->middleware('checktoken');
Route::post('product-del','ProductController@delete')->middleware('checktoken');
Route::post('product-stock','ProductController@updatestock')->middleware('checktoken');

// Route::post('product-stock-log','ProductStockLogsController@create')->middleware('checktoken');
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
