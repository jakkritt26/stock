<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'product_stock';
    protected $fillable = [
        'name', 'price', 'stock','img','status','unit',
    ];
}
