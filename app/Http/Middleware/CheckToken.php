<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_check = User::where('api_token',$request->header('Authorization'))->first();
        if($user_check)
            {
                $response = $next( $request );
                $response->header('Access-Control-Allow-Origin', '*');
                $response->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
                $response->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token-Auth, Authorization');
                                
                return $response;
            }else{
                return response()->json(['message'=>'token invalid','status'=>'0']);
            }

        // $requestData = $request->all();
        // //print_r($requestData);
        
        // if( $user_check){
        //     return $next($request);
        // }else{
        //     return false;
        // }*/
        
    }
}
