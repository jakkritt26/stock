<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Validator;
class ProductController extends Controller
{

  public function list2(Request $request){
    $rules = [
      'page' => 'required'
      
  ];

  $validator = Validator::make($request->all(),$rules);
  if($validator->fails()){
      return response()->json($validator->errors());
  }

    $requestData = $request->all();
    $limit =20;
    //if( $requestData['page']!=""){
      $start = (($requestData['page']-1)*20)+1;
      $res = Product::where('status','=', 1)->orderBy('id', 'desc')->offset($start)->limit($limit)->get();
  //  }
   // }else{
    //  $res = Product::where('status','=', 1)->get();
   // }
    
    $return_data = array();
    $return_data['data'] = $res;
    return response()->json($return_data,200);

  }

  public function list(Request $request){
    $res = Product::where('status','=', 1)->select('id','name','price','stock')->latest()->get();
    $return_data = array();
    $return_data['data'] = $res;
    return response()->json($return_data,200);

  }
  public function get($id){

    $return_data = array();
    if(!$id){
      $return_data['text'] = 'required id';
      $return_data['status'] = 400;
      return response()->json($return_data,200);
    }
    
    $pro = Product::where('id', $id)->get();
    if($pro){
      $return_data['status'] = 1;
      $return_data['data']  =$pro;
     
    }else{
      $return_data['status'] = 400;
    }
    return response()->json($return_data,200);
  }

  public function create(Request $request){
       
        $rules = [
            'name' => 'required',
            'price' => 'required',
            
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json($validator->errors());
        }

        $res = Product::create($request->all());
        $return_data = array();
        
       if($res){
         $return_data['status'] = 1;
        
       }else{
         $return_data['status'] = 400;
       }
       return response()->json($return_data,200);
    }


    public function update(Request $request){
        $rules = [            
            'name' => 'required',
            'price' => 'required',   
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json($validator->errors());
        }
        
        $requestData = $request->all();
        $pro = Product::where('id', $requestData['id'])
              ->update(['name' =>$requestData['name'],'price' =>$requestData['price']
              ,'unit' =>$requestData['unit'],'stock' =>$requestData['stock']]);
        $return_data = array();
        
       if($pro){
         $return_data['status'] = 1;
        
       }else{
         $return_data['status'] = 400;
       }
       return response()->json($return_data,200);
    }

    
    public function updatestock(Request $request){
        $rules = [           
            'stock'   => 'required', 
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json($validator->errors());
        }
        
        $requestData = $request->all();
        $pro = Product::where('id', $requestData['id'])
              ->update(['stock' =>$requestData['stock']]);
        $return_data = array();
        
       if($pro){
         $return_data['status'] = 1;
        
       }else{
         $return_data['status'] = 400;
       }
       return response()->json($return_data,200);
    }

    public function delete(Request $request){
       
        //  $rules = [
        //      'id' => 'required',         
             
        //  ];

        //  $validator = Validator::make($request->all(),$rules);
        // if($validator->fails()){
        //     return response()->json($validator->errors());
        // }
        
        $requestData = $request->all();
        $pro = Product::where('id', $requestData['id'])
              ->update(['status' =>0]);
        
 
       /**/
 
        // $res = Product::create($request->all());
         $return_data = array();
         
        if($pro){
          $return_data['status'] = 1;
         
        }else{
          $return_data['status'] = 400;
        }
        return response()->json($return_data,200);
     }
}
