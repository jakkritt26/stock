<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Validator;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json($validator->errors());
        }

        $requestData = $request->all();
        $email = $requestData['email'];
        $password = $requestData['password'];
        
            $user_check = User::where('email',$email)->first();
            if(!$user_check){
                $return_data['status'] = 0;
                $return_data['message'] = 'User Not Found';
                return response()->json($return_data);
            }
            $user_array = $user_check->toArray();
          // echo $user_check->password;
            $return_data = array();       
           // if(count($user_array)>0){
                $user_check_passs  = Hash::check($password, $user_check->password);
                if($user_check_passs){
                    $return_data['data']  = $user_check;
                    $return_data['status'] = 1;
                    $return_data['auth'] = 1;
                return response()->json($return_data);
                }else{
                    return response()->json(['message'=>'Password incorrect','status'=>'200']);
                }
            // }else{
            //     $return_data['status'] = 400;
            //     $return_data['message'] = 'User Not Found';
            //     return response()->json($return_data);
            // }
    }

    public function logout(Request $request){
        
    }
}
