<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logs;
use Validator;

class ProductStockLogsController extends Controller
{
    public function create(Request $request){
       
        $rules = [
            'product_id' => 'required',
            'stock' => 'required',
            'user' => 'required',
            
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json($validator->errors());
        }

        $res = Logs::create($request->all());
        $return_data = array();
        
       if($res){
         $return_data['status'] = 200;
        
       }else{
         $return_data['status'] = 400;
       }
       return response()->json($return_data,200);
    }
}
