<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    
    public function create(Request $request)
            {

                $requestData = $request->all();
                $request->merge([
                 'password' => Hash::make($requestData['password']),
                 'api_token'=> Str::random(80),
                 ]);
               
                $user = User::create($request->all());
                $return_data = array();
                
               if($user){
                 $return_data['status'] = 200;
                
               }else{
                 $return_data['status'] = 400;
               }
               return response()->json($return_data,200);

            }

            public function get(Request $request){
              $requestData = $request->all();
              if(!isset($requestData['id'])){
                $return_data['status'] = 400;
                return response()->json($return_data);
              }

              $user = User::where('id',$requestData['id'])->first();
              $return_data = array();
              if($user){
                $return_data['data'] = $user;
                $return_data['status'] = 200;
                $return_data['auth'] = 1;
              }else{
                $return_data['status'] = 400;
              }
              return response()->json($return_data);
            }
    }
